Polymer({
    is: 'lf-radio-button-group',

    properties: {
        buttonDefinitions: {
            type: Array
        },
        groupValue: {
            type: String
        },
        clickHandler: {
            type: Object
        }
    },

    computeClass: function(btnValue, groupValue) {
        var baseClass = "pull-left lf-radio-button";

        if (btnValue.toLowerCase() === groupValue.toLowerCase()) {
            return baseClass + " lf-radio-button-active";
        } else {
            return baseClass;
        }
    },
    ready:function(){
      console.log("radio-group ready called");
    },
    handleClick: function(ev) {
        var paymentMethod = ev.model.btn.value;
        this.clickHandler(paymentMethod);
    }

});
