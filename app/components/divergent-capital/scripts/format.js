var formats = {
    dateFormats: {
        USA: {
            dateFormatString: 'MM/DD/YYYY',
            dateTimeFormatString: 'MM/DD/YYYY hh:mm:ss a'
        }
    },
    ssnMaskformate: '*****XXXX',
    currencySymbols: {
        USA: '$'
    },
    phoneFormats: {
        USA: {
            maskFormat: '(XXX)-XXX-XXXX'
        }
    },
    validateInput: function (value) {
        if (value == null || value == undefined || value == '') {
            return false;
        } else {
            return true;
        }
    },
    date: function (dateValue, country, formatWithTime) {
        var self = this;
        if (!self.validateInput(dateValue)) {
            return dateValue;
        }
        if (!self.validateInput(country)) {
            return moment.tz(dateValue, window.config.timezone);
        }
        if (!self.validateInput(self.dateFormats[country])) {
            return moment.tz(dateValue, window.config.timezone);
        }
        if (formatWithTime) {
            return moment.tz(dateValue, window.config.timezone).format(self.dateFormats[country].dateTimeFormatString);
        } else {
            return moment.tz(dateValue, window.config.timezone).format(self.dateFormats[country].dateFormatString);
        }
    },
    money: function (amount, country, decimalPoint) {
        var self = this;
        if (!self.validateInput(amount) || !self.validateInput(country) || !self.validateInput(self.dateFormats[country])) {
            return amount;
        } else {
            var currency = parseFloat(amount);
            var currencyformate = self.currencySymbols[country] + '1,'
            return self.currencySymbols[country] + currency.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, currencyformate);
        }
    },
    mask: function (mask, input) {
        var s = '' + input,
            result = '';
        for (var im = 0, is = 0; im < mask.length && is < s.length; im++) {
            if (mask.charAt(im) == 'X') {
                result += s.charAt(is);
                is++;
            } else if (mask.charAt(im) == '*') {
                result += mask.charAt(im);
                is++;
            } else {
                result += mask.charAt(im);
            }
        }
        return result;
    },
    phone: function (phonNumber, country) {
        var self = this;
        if (!self.validateInput(phonNumber) || !self.validateInput(country) || !self.validateInput(self.phoneFormats[country])) {
            return phonNumber;
        }
        var actualPhoneNumber;
        try {
            if (phonNumber.indexOf("-") != -1) {
                actualPhoneNumber = phonNumber.replace(/-/g, "");
            } else {
                actualPhoneNumber = phonNumber;
            }
            return self.mask(self.phoneFormats[country].maskFormat, actualPhoneNumber);
        }
        catch (e) {
            return actualPhoneNumber;
        }
        return '';
    },
    ssn: function (ssnNumber) {
        var self = this;
        if (!self.validateInput(ssnNumber)) {
            return ssnNumber;
        }
        var ssnformat = self.ssnMaskformate;
        return self.mask(ssnformat, ssnNumber);
    },
    round: function (value, decimalPoint) {
        var self=this;
        if (self.validateInput(value)) {
            value = parseFloat(value);
            if (self.validateInput(decimalPoint)) {
                return value.toFixed(decimalPoint);
            }
            else {
                return value.toFixed(0);
            }
        }
        else {
            return 0.00;
        }
    }
}
