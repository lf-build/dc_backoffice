var express = require('express');
var router = express.Router();
var AWS = require('aws-sdk');
var jwt = require('jsonwebtoken');

// AWS Configuration from ENV variables
// AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_SESSION_TOKEN (optional)

var s3 = new AWS.S3();

var getApp = function(tenant, path, callback) {
    var params = {
        Bucket: tenant + '/',
        Key: path
    }

    s3.getObject(params, callback);
};

var getAppCurry = function(tenant) {
    return function(path, callback) {
        getApp(tenant, path, callback);
    }
}


router.get('/:tenant/:path(*)', function(req, res) {
    var tenant = req.params.tenant;
    var filePath = req.params.path;

    var getDefaultApp = getAppCurry("lendfoundry");
    var getTenantApp = getAppCurry(tenant);

    getTenantApp(tenant, filePath, function(err, data) {
        if (err) {
            getDefaultApp(filePath, function(data) {
                if (err) {
                    res.status(404).send("File not found.");
                } else {
                    res.type('html').send(data);
                }
            });
        } else {
            res.type('html').send(data);
        }
    })
});

module.exports = router;