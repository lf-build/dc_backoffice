var gulp = require("gulp");
//var browserSync = require("browser-sync");
var bs = require('browser-sync').create();

// // Watch files for changes & reload
// gulp.task('test', function () {
//     browserSync.init({
//         port: 5000,
//         server: {
//             baseDir: './'
//         },
//         startPath: "/test/index.html"
//     });
//
//     gulp.watch(
//         [
//             "./components/**/*.*",
//             "./test/**/*.*"
//         ]
//     ).on("change", browserSync.reload)
// });

gulp.task('dev', function () {
    bs.init({
        port: 5052,
        proxy: 'http://localhost:5152',
         files: ['./app'],
        snippetOptions: {
            // Ignore all HTML files within the templates folder
            rule: {
                match: /<\/bs>/i,
                fn: function (snippet, match) {
                  console.log(snippet);
                  snippet = "<script id=\"__bs_script__\"> document.write('<script src=\"http://localhost:5052/browser-sync/browser-sync-client.2.15.0.js\" ><\\/script>'); </script>";
                  console.log('bs', snippet, match);
                  return snippet + match;
                }
            }
        },
        startPath: "/"
    });

   
 });
