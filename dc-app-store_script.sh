# app-store image creation script
#sh dc-app-store_script.sh



cd app/components/
rm divergent-capital
ln -s BL divergent-capital
cd ../../

docker build --no-cache -t registry.lendfoundry.com/dc-app-store:dev .

docker push registry.lendfoundry.com/dc-app-store:dev
